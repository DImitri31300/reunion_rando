<?php
session_start();
$bdd = new PDO('mysql:host=127.0.0.1;dbname=reunion_island', 'root', '');




if(isset($_POST['button'])){
	$name = $_POST['name'];
	$difficulty = $_POST['difficulty'];
	$distance = $_POST['distance'];
	$duree = $_POST['duration'];
	$denivele = $_POST['height_difference'];

	$insertrando = $bdd->prepare('INSERT INTO hiking (name, difficulty, distance, duration, height_difference) VALUES (?, ?, ?, ?, ?);');
	$insertrando->execute(array($name, $difficulty, $distance, $duree, $denivele));
	
	$randoinfo = $req->fetch();
	$_SESSION['name'] = $randoinfo['name'];
	$_SESSION['difficulty'] = $randoinfo['difficulty'];
	$_SESSION['distance'] = $randoinfo['distance'];
	$_SESSION['duration'] = $randoinfo['duration'];
	$_SESSION['height_difference'] = $randoinfo['height_difference'];
	echo '<p class="bravo"> Votre randonnée a bien été enregistré .</p></p>';	
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ajouter une randonnée</title>
	<link rel="stylesheet" href="css/basics.css" media="screen" title="no title" charset="utf-8">
</head>
<body>
	<a href="read.php">Liste des randonnées</a>
	<h1>Ajouter</h1>
	<form action="" method="POST">
		<div>
			<label for="name">Name</label>
			<input type="text" name="name" value="">
		</div>

		<div>
			<label for="difficulty">Difficulté</label>
			<select name="difficulty">
				<option value="très facile">Très facile</option>
				<option value="facile">Facile</option>
				<option value="moyen">Moyen</option>
				<option value="difficile">Difficile</option>
				<option value="très difficile">Très difficile</option>
			</select>
		</div>
		
		<div>
			<label for="distance">Distance</label>
			<input type="text" name="distance" value="">
		</div>
		<div>
			<label for="duration">Durée</label>
			<input type="duration" name="duration" value="">
		</div>
		<div>
			<label for="height_difference">Dénivelé</label>
			<input type="text" name="height_difference" value="">
		</div>
		<button type="submit" name="button">Envoyer</button>
	</form>
</body>
</html>

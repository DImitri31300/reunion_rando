<?php
session_start();

$bdd = new PDO('mysql:host=127.0.0.1;dbname=reunion_island','root', '');

if(isset($_POST['formconnexion']))
{
    $mailconnect = htmlspecialchars($_POST['mailconnect']);
    $mdpconnect  = sha1($_POST['mdpconnect']);
    if(!empty($mailconnect) AND !empty($mdpconnect)) 
    {
        $requser = $bdd->prepare("SELECT * FROM membres WHERE mail = ? AND mdp = ? ");
        $requser->execute(array($mailconnect, $mdpconnect));
        $userexist = $requser->rowCount();
        if($userexist == 1)
        {
            $userinfo = $requser->fetch();
            $_SESSION['id_user']     = $userinfo['id_user'];
            $_SESSION['pseudo'] = $userinfo['user'];
            $_SESSION['mail']   = $userinfo['mail'];
            header("Location: read.php?id=".$_SESSION['id']);
        }
        else
        {
            $erreur = "Mauvais mail ou mot de passe";
        }
    }
    else
    {
        $erreur = "Tous les champs doivent êtres complétés";
    }
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Espace membre tuto php</title>
</head>
<body>
<div align="center">
    <h2>Connexion</h2>
    <br><br><br>
    <form action="" method="POST">
      <input type="email"    name="mailconnect"   placeholder="Mail">
      <input type="password" name="mdpconnect"    placeholder="Mot de passe">
      <input type="submit"   name="formconnexion" value="Se connecter">
    </form>
    <br>
    <a href="inscription.php">Pas de compte ? Enregistrer vous !</a>
    <?php 
        if(isset($erreur)) {
            echo '<br><font color="red">' . $erreur . '</font>';
        }
    ?>
</div>
    
</body>
</html>
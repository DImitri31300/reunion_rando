<?php

$bdd = new PDO('mysql:host=127.0.0.1;dbname=reunion_island','root', '');

if(isset($_POST['forminscription'])) {
    $pseudo  = htmlspecialchars($_POST['pseudo']);
    $mail    = htmlspecialchars($_POST['mail']);
    $mail2   = htmlspecialchars($_POST['mail2']);
    $mdp     = sha1($_POST['mdp']);
    $mdp2    = sha1($_POST['mdp2']);
    if(!empty($_POST['pseudo']) AND !empty($_POST['mail']) AND !empty($_POST['mail2']) AND !empty($_POST['mdp']) AND !empty($_POST['mdp2'])) {

        $pseudolength = strlen($pseudo);
        if($pseudolength <= 255) {
            if($mail == $mail2) {
                if(filter_var($mail, FILTER_VALIDATE_EMAIL)) {
                    $reqmail = $bdd->prepare("SELECT * FROM membres WHERE mail = ?");
                    $reqmail->execute(array($mail));
                    $mailexist = $reqmail->rowCount();
                    if($mailexist == 0) {
                        if($mdp == $mdp2) {
                            $insertmbr = $bdd->prepare('INSERT INTO membres(user, mail, mdp) VALUES(?, ?, ?)');
                            $insertmbr->execute(array($pseudo, $mail, $mdp));
                            $erreur = "Votre compte à bien été créé ! <a href=\"login.php\">Me connecter</a>";
                        } else {
                            $erreur = "Vos mots de passe ne correspondent pas .";
                        }
                    } else {
                        $erreur = "Adresse mail déja utilisée";
                    }
                    } else {
                    $erreur = "Votre adress mail n'est pas vailde";
                }
                } else {
                    $erreur ="Vos adresses mails ne correspondent pas .";
                }
        } else {
            $erreur = "Votre pseudo ne doit pas dépasser 255 caractères";
        }
    } else {
        $erreur = "Tous les champs doivent être complétés !";
    }
}

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Espace membre tuto php</title>
</head>
<body>
<div align="center">
    <h2>Inscription</h2>
    <br><br><br>
    <form action="" method="POST">
        <table>
            <tr>
                <td align="right">
                  <label for="pseudo">Votre pseudo :</label>
                </td>
                <td>
                  <input type="text" name="pseudo" placeholder="Votre pseudo" id="pseudo" value="<?php if(isset($pseudo)) { echo $pseudo; } ?>" >
                </td>
            </tr>
            <tr>
                <td align="right">
                  <label for="mail">Votre E-mail :</label>
                </td>
                <td>
                  <input type="email" name="mail" placeholder="Votre mail" id="mail" value="<?php if(isset($mail)) { echo $mail; } ?>"  >
                </td>
            </tr>
            <tr>
                <td align="right">
                  <label for="mail2">Confirmation E-mail :</label>
                </td>
                <td>
                  <input type="email" name="mail2" placeholder="Confirmez votre mail" id="mail2" value="<?php if(isset($mail2)) { echo $mail2; } ?>" >
                </td>
            </tr>
            <tr>
                <td align="right">
                  <label for="mdp">Entrez un mot de passe :</label>
                </td>
                <td>
                  <input type="password" name="mdp" placeholder="Votre mot de passe" id="mdp">
                </td>
            </tr>
            <tr>
                <td align="right">
                  <label for="mdp2">Confirmez votre mot de passe :</label>
                </td>
                <td>
                  <input type="password" name="mdp2" placeholder="Confirmez ici" id="mdp2">
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                <input type="submit" value="Je m'inscris" name="forminscription">
                </td>
            </tr>
        </table>
    </form>
    <?php 
        if(isset($erreur)) {
            echo '<font color="red">' . $erreur . '</font>';
        }
    ?>
</div>
    
</body>
</html>
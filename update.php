<?php
session_start();
$bdd = new PDO('mysql:host=127.0.0.1;dbname=reunion_island', 'root', '');

$id = $_GET['id'];

$req = $bdd->prepare("SELECT *FROM hiking WHERE id =  $id ");
$req->execute(array($id));


$info = $req->fetch();
$_SESSION['name']              = $info['name'];
$_SESSION['difficulty']        = $info['difficulty'];
$_SESSION['distance']          = $info['distance'];
$_SESSION['duration']          = $info['duration'];
$_SESSION['height_difference'] = $info['height_difference'];


echo  '<h1>' . $_GET['id'] .'</h1>';


if(isset($id)) {
	$reqrando = $bdd->prepare("SELECT *FROM hiking WHERE id =  $id ");
	$reqrando->execute((array($id)));
	$infobdd = $reqrando->fetch();
	if(isset($_POST['btn'])) {
		if(isset($_POST['newname'])) {
			$newname = ($_POST['newname']);
			$insertnewname = $bdd->prepare("UPDATE hiking SET name = ? WHERE id = ?");
			$insertnewname->execute(array($newname, $_GET['id']));
			header('Location: read.php');
		}
		if(isset($_POST['newdifficulty'])) {
			$newdifficulty = ($_POST['newdifficulty']);
			$insertnewdifficulty = $bdd->prepare("UPDATE hiking SET difficulty = ? WHERE id = ?");
			$insertnewdifficulty->execute(array($newdifficulty, $_GET['id']));
			header('Location: read.php');
		}
		if(isset($_POST['newdistance'])) {
			$newdistance = ($_POST['newdistance']);
			$insertnewdistance = $bdd->prepare("UPDATE hiking SET distance = ? WHERE id = ?");
			$insertnewdistance->execute(array($newdistance, $_GET['id']));
			header('Location: read.php');
		}
		if(isset($_POST['newduration'])) {
			$newduration = ($_POST['newduration']);
			$insertnewduration = $bdd->prepare("UPDATE hiking SET duration = ? WHERE id = ?");
			$insertnewduration->execute(array($newduration, $_GET['id']));
			header('Location: read.php');
		}
		if(isset($_POST['newheight_difference'])) {
			$newheight_difference = ($_POST['newheight_difference']);
			$insertnewheight_difference = $bdd->prepare("UPDATE hiking SET height_difference = ? WHERE id = ?");
			$insertnewheight_difference->execute(array($newheight_difference, $_GET['id']));
			header('Location: read.php');
		}
	}
}




?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ajouter une randonnée</title>
	<link rel="stylesheet" href="css/basics.css" media="screen" title="no title" charset="utf-8">
</head>
<body align="center">
	<a href="read.php">Liste des randonnées</a>
	<h1>Ajouter</h1>
	<form action="" method="POST" name="btn">
		<div>
			<label for="name">Name</label>
			<input type="text" name="newname" value="<?php echo $_SESSION['name']; ?>">
		</div>

		<div>
			<label for="difficulty">Difficulté</label>
			<select name="newdifficulty" value="<?php echo $_SESSION['difficulty']; ?>">
				<option value="très facile">Très facile</option>
				<option value="facile">Facile</option>
				<option value="moyen">Moyen</option>
				<option value="difficile">Difficile</option>
				<option value="très difficile">Très difficile</option>
			</select>
		</div>
		
		<div>
			<label for="distance">Distance</label>
			<input type="text" name="newdistance" value="<?php echo $_SESSION['distance']; ?>">
		</div>
		<div>
			<label for="duration">Durée</label>
			<input type="duration" name="newduration" value="<?php echo $_SESSION['duration']; ?>">
		</div>
		<div>
			<label for="height_difference">Dénivelé</label>
			<input type="text" name="newheight_difference" value="<?php echo $_SESSION['height_difference']; ?>">
		</div>

		<input type="submit" name="btn" value="Envoyer">
	</form>
</body>
</html>
